from django.db import models
from django.urls import reverse
from django.core.serializers.json import DjangoJSONEncoder


class Attendee(models.Model):
    """
    The Attendee model represents someone that wants to attend
    a conference
    """

    email = models.EmailField()
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    conference = models.ForeignKey(
        "events.Conference",
        related_name="attendees",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"id": self.id})

    def create_badge(self):
        try:
            self.badge
        except ObjectDoesNotExist:
            Badge.objects.create(attendee=self)

class Badge(models.Model):
    """
    The Badge model represents the badge an attendee gets to
    wear at the conference.

    Badge is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    created = models.DateTimeField(auto_now_add=True)

    attendee = models.OneToOneField(
        Attendee,
        related_name="badge",
        on_delete=models.CASCADE,
        primary_key=True,
    )

# Attendee JSON Encoder
class AttendeeDetailEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Attendee):
            return {
                "id": obj.id,
                "email": obj.email,
                "name": obj.name,
                "company_name": obj.company_name,
                "created": obj.created,
                "conference_id": obj.conference.id if obj.conference else None,
                "api_url": obj.get_api_url()
            }
        return super().default(obj)

# Badge JSON Encoder
class BadgeEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, Badge):
            return {
                "created": obj.created,
                "attendee_id": obj.attendee.id if obj.attendee else None
            }
        return super().default(obj)
