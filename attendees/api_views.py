from django.http import JsonResponse
from .models import Attendee
from django.urls import reverse
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from events.models import Conference
from events.api_views import ConferenceListEncoder


# def api_list_attendees(request, conference_id):
"""
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.
    """
    # attendees = Attendee.objects.filter(conference_id=conference_id)
    # attendees_list = [
    #     {
    #     "name": attendee.name,
    #     "href": reverse('api_show_attendee', args=[attendee.id])
    # }for attendee in attendees]

    # return JsonResponse({"attendees": attendees_list})

class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name",
                "href",
    ]

    def default(self, o):
        if isinstance(o, Attendee):
            data = super().default(o)
            data["href"] = reverse('api_show_attendee', args=[o.id])
            return data
        else:
            return super().default(o)

class AttendeeEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created", "conference"]

    def default(self, o):
        if isinstance(o, Attendee):
            data = super().default(o)
            data["conference"] = {
                "name": o.conference.name,
                "href": reverse('api_show_conference', args=[o.conference.id])
            } if o.conference else None
            return data
        else:
            return super().default(o)


@require_http_methods(["GET", "POST"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

    attendee = Attendee.objects.create(**content)
    return JsonResponse(
        attendee,
        encoder=AttendeeEncoder,
        safe=False,
    )



    attendees = Attendee.objects.filter(conference=conference_id)
    return JsonResponse(
        {"attendees": attendees},
        encoder=AttendeeListEncoder,
    )


# def api_show_attendee(request, id):
    """
    Returns the details for the Attendee model specified
    by the id parameter.

    This should return a dictionary with email, name,
    company name, created, and conference properties for
    the specified Attendee instance.

    {
        "email": the attendee's email,
        "name": the attendee's name,
        "company_name": the attendee's company's name,
        "created": the date/time when the record was created,
        "conference": {
            "name": the name of the conference,
            "href": the URL to the conference,
        }
    }
    """
    # attendee = Attendee.objects.get(id=id)
    # response_data = {
    #     "email": attendee.email,
    #     "name": attendee.name,
    #     "company_name": attendee.company_name,
    #     "created": attendee.created,
    #     "conference": {
    #         "name": attendee.conference.name,
    #         "href": reverse('api_show_conference', args=[attendee.conference.id])
    #     }
    # }
    # return JsonResponse(response_data)

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return JsonResponse({"message": "Attendee not found"}, status=404)

    if request.method == "GET":
        return JsonResponse(attendee, encoder=AttendeeEncoder, safe=False)

    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            setattr(attendee, key, value)
        attendee.save()
        return JsonResponse(attendee, encoder=AttendeeEncoder, safe=False)

    elif request.method == "DELETE":
        attendee.delete()
        return JsonResponse({"message": "Attendee deleted"}, status=204)
