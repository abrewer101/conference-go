from django.http import JsonResponse
from .models import Conference, Location, ConferenceEncoder, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data
from .keys import OPEN_WEATHER_API_KEY
"""
    Lists the conference names and the link to the conference.

    Returns a dictionary with a single key "conferences" which
    is a list of conference names and URLS. Each entry in the list
    is a dictionary that contains the name of the conference and
    the link to the conference's information.

    {
        "conferences": [
            {
                "name": conference's name,
                "href": URL to the conference,
            },
            ...
        ]
    }
    """

class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return { "state": o.state.abbreviation }


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conference": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)

    # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name"]

class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }

    """
    Returns the details for the Conference model specified
    by the id parameter.

    This should return a dictionary with the name, starts,
    ends, description, created, updated, max_presentations,
    max_attendees, and a dictionary for the location containing
    its name and href.

    {
        "name": the conference's name,
        "starts": the date/time when the conference starts,
        "ends": the date/time when the conference ends,
        "description": the description of the conference,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "max_presentations": the maximum number of presentations,
        "max_attendees": the maximum number of attendees,
        "location": {
            "name": the name of the location,
            "href": the URL for the location,
        }
    }
    """
    # conference = Conference.objects.get(id=id)
    # return JsonResponse(
    #     {
    #         "name": conference.name,
    #         "starts": conference.starts,
    #         "ends": conference.ends,
    #         "description": conference.description,
    #         "created": conference.created,
    #         "updated": conference.updated,
    #         "max_presentations": conference.max_presentations,
    #         "max_attendees": conference.max_attendees,
    #         "location": {
    #             "name": conference.location.name,
    #             "href": conference.location.get_api_url(),
    #         },
    #     }
    # )

@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):

    # city = conference.location.city
    # state = conference.location.state

    try:
        conference = Conference.objects.get(id=id)
    except Conference.DoesNotExist:
        return JsonResponse({"message": "Conference not found"}, status=404)

    if request.method == "GET":
        return JsonResponse(conference, encoder=ConferenceEncoder, safe=False)

    elif request.method == "PUT":
        content = json.loads(request.body)
        for key, value in content.items():
            setattr(conference, key, value)
        conference.save()
        return JsonResponse(conference, encoder=ConferenceEncoder, safe=False)

    elif request.method == "DELETE":
        conference.delete()
        return JsonResponse({"message": "Conference deleted"}, status=204)

@require_http_methods(["GET"])
def api_get_weather(request):
    # Assuming city and state are passed as query parameters
    city = request.GET.get('city')
    state = request.GET.get('state')

    if not city or not state:
        return JsonResponse({"error": "City and state parameters are required"}, status=400)

    weather_data = get_weather_data(city, state)

    if weather_data == "Location not found":
        return JsonResponse({"error": "Location not found"}, status=404)

    return JsonResponse(weather_data)

    """
    Lists the location names and the link to the location.

    Returns a dictionary with a single key "locations" which
    is a list of location names and URLS. Each entry in the list
    is a dictionary that contains the name of the location and
    the link to the location's information.

    {
        "locations": [
            {
                "name": location's name,
                "href": URL to the location,
            },
            ...
        ]
    }
    """
# def api_list_locations(request):
#     response = []
#     locations = Location.objects.all()
#     for location in locations:
#         response.append(
#             {
#                 "name": location.name,
#                 "href": location.get_api_url(),
#             }
#         )
#     return JsonResponse({"locations": response})

@require_http_methods(["GET", "POST"])
def api_list_locations(request):

    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": list(locations)},
            encoder=LocationListEncoder
        )
    else:
        content = json.loads(request.body)

    # Get the State object and put it in the content dict
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        photo = get_photo(content["state"].abbreviation, content["city"])
        content.update(photo)
        location = Location.objects.create(**content)
    # Get the State object and put it in the content dict
    # state = State.objects.get(abbreviation=content["state"])
    # content["state"] = state
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )




    """
    Returns the details for the Location model specified
    by the id parameter.

    This should return a dictionary with the name, city,
    room count, created, updated, and state abbreviation.

    {
        "name": location's name,
        "city": location's city,
        "room_count": the number of rooms available,
        "created": the date/time when the record was created,
        "updated": the date/time when the record was updated,
        "state": the two-letter abbreviation for the state,
    }
    """

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    try:
        location = Location.objects.get(id=id)
    except Location.DoesNotExist:
        return JsonResponse({"message": "Location not found"}, status=404)

    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:  # Handling PUT request
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

    Location.objects.filter(id=id).update(**content)

    location = Location.objects.get(id=id)
    return JsonResponse(
        location,
        encoder=LocationDetailEncoder,
        safe=False,
    )


@require_http_methods(["GET"])
def weather_data_view(request, city, state):

    weather_info = get_weather_data(city, state, OPEN_WEATHER_API_KEY)

    return JsonResponse(weather_info)
