from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(city, state):
    # Use the Pexels API
    url = "https://api.pexels.com/v1/search"

    headers = {"Authorization": PEXELS_API_KEY}
    params = {"per_page": 1, "query":city + " " + state }

    response = requests.get(url, headers=headers,params=params)
    content = json.loads(response.content)
    content_photos = content["photos"]

    try:
        return {"picture_url": content_photos[0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}

    # if response.status_code == 200:
    #     data = response.json()
    #     if data["photos"]:
    #         picture_url = data["photos"][0]["src"]["medium"]

    #     return {'picutre_url': picture_url}
    # else:
    #     return {'error'}




def get_weather_data(city, state):
    OPEN_WEATHER_API_KEY = keys.OPEN_WEATHER_API_KEY
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(geo_url, header=header,params=params)
    # coords = response.json()
    header = {""}
    params = {"q": city + "," + state + "," + "US", "appid": OPEN_WEATHER_API_KEY}
    response_data = response.json()


    # Extract latitude and longitude
    if not response_data:
        return "Location not found"
    latitude = response_data[0]['lat']
    longitude = response_data[0]['lon']

    # Get Current Weather Data
    weather_url = f"http://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}&units=metric"
    response = requests.get(weather_url)
    weather_data = response.json()

    # Extract Temperature and Weather Description
    main_temp = weather_data['main']['temp']
    weather_description = weather_data['weather'][0]['description']

    # Return the Data
    return {
        'temperature': main_temp,
        'description': weather_description
    }










    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
