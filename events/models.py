from django.db import models
from django.urls import reverse
from django.core.serializers.json import DjangoJSONEncoder


class State(models.Model):
    """
    The State model represents a US state with its name
    and abbreviation.

    State is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    name = models.CharField(max_length=20)
    abbreviation = models.CharField(max_length=2)

    def __str__(self):
        return f"{self.abbreviation}"

    class Meta:
        ordering = ("abbreviation",)  # Default ordering for State

class StateEncoder(DjangoJSONEncoder):

    def default(self, obj):
        if isinstance(obj, State):
            return {
                "id": obj.id,
                "name": obj.name,
                "abbreviation": obj.abbreviation
            }
        return super().default(obj)


class Location(models.Model):
    """
    The Location model describes the place at which an
    Event takes place, like a hotel or conference center.
    """

    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200)
    room_count = models.PositiveSmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    picture_url = models.URLField(null=True)

    state = models.ForeignKey(
        State,
        related_name="+",  # do not create a related name on State
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_location", kwargs={"id": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("name",)  # Default ordering for Location

# class LocationEncoder(DjangoJSONEncoder):

#     def default(self, obj):
#         if isinstance(obj, Location):
#             return {
#                 "id": obj.id,
#                 "name": obj.name,
#                 "city": obj.city,
#                 "room_count": obj.room_count,
#                 "created": obj.created,
#                 "updated": obj.updated,
#                 "state": obj.state.id if obj.state else None,
#                 # "api_url": obj.get_api_url()
#             }
#         return super().default(obj)


class Conference(models.Model):
    """
    The Conference model describes a specific conference.
    """

    # Has a one-to-many relationship with presentations.Presentation
    # Has a one-to-many relationship with attendees.Attendee

    name = models.CharField(max_length=200)
    starts = models.DateTimeField()
    ends = models.DateTimeField()
    description = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    max_presentations = models.PositiveSmallIntegerField()
    max_attendees = models.PositiveIntegerField()

    location = models.ForeignKey(
        Location,
        related_name="conferences",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_conference", kwargs={"id": self.id})

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("starts", "name")  # Default ordering for Conference

class ConferenceEncoder(DjangoJSONEncoder):

    def default(self, obj):
        if isinstance(obj, Conference):
            return {
                "id": obj.id,
                "name": obj.name,
                "starts": obj.starts,
                "ends": obj.ends,
                "description": obj.description,
                "created": obj.created,
                "updated": obj.updated,
                "max_presentations": obj.max_presentations,
                "max_attendees": obj.max_attendees,
                "location": obj.location.id if obj.location else None,
                "api_url": obj.get_api_url()
            }
        return super().default(obj)
